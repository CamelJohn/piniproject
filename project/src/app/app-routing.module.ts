import { HodgesonMooreComponent } from './orders/hodgeson-moore/hodgeson-moore.component';
import { EddComponent } from './orders/edd/edd.component';
import { FifoComponent } from './orders/fifo/fifo.component';
import { FormComponent } from './orders/form/form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'form', component: FormComponent },
  { path: 'fifo', component: FifoComponent },
  { path: 'edd', component: EddComponent },
  { path: 'hm', component: HodgesonMooreComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
