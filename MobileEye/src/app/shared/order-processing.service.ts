import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderProcessingService {

  constructor() { }

  checkPrefs = (orderPref) => {
    if (orderPref.includes('&')) {
      return 'qty & color';
    }
    if (orderPref.includes('date')) {
      return 'date';
    } else if (orderPref.includes('color')) {
      return 'color';
    } else if (orderPref.includes('order')) {
      return 'qty';
    }  else {
      return 'none';
    }
 }

  checkTier(qty) {
    let tier;
    if (qty <= 1000) {
      tier = 'tier1';
    } else if (qty <= 5000) {
     tier = 'tier2';
    } else if (qty <= 10000) {
     tier = 'tier3';
    } else if (qty > 10000) {
     tier = 'tier4';
    }
    return tier;
  }

 checkRange = (delivery): number => {
  const d2 = new Date(delivery);
  const d1 = new Date();
  const totalDays = (Math.abs(Date.UTC(d2.getFullYear(), d2.getMonth(), d2.getDate()) -
  Date.UTC(d1.getFullYear(), d1.getMonth(), d1.getDate())) /
  (1000 * 60 * 60 * 24));

  return totalDays;
  }
}
