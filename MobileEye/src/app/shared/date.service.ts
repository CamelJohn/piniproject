import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  quarters = ['first quarter', 'second quarter', 'third quarter', 'fourth quarter', 'first half', 'second half', 'flexible'];

  constructor() { }

  myDateFilter = (
    d: Date,
    dateRange,
    deliveryRange,
    minMonth,
    maxMonth,
    minDate,
    maxDate) => {
    const month = d.getMonth();
    const year = d.getFullYear();
    const range = dateRange;
    let res;
    let min;
    let max;
    for (let i = 0; i < deliveryRange.length; i++) {
      if (range === deliveryRange[i] && i === 0) {
        min = i;
        max = min + 3;
      } else if (range === deliveryRange[i] && i <= 3) {
         min = i * 3;
         if (range === deliveryRange[i] && i === 1) {
        max = i * 5;
        } else if (range === deliveryRange[i] && i === 2) {
          max = i * 2;
        } else if (range === deliveryRange[i] && i === 3) {
          max = i + 8;
        }
      } else if (range === deliveryRange[i] && i === 4) {
        min = i - i;
        max = i + 1;
      } else if (range === deliveryRange[i] && i === 5) {
        min = i + 1;
        max = i + 6;
      } else if (range === deliveryRange[i] && i === 6) {
        min = i - i;
        max = i + 5;
      }
    }
    res = (month >= min && month <= max);
    minMonth = min;
    maxMonth = max;
    minDate = new Date(year, min, 1);
    maxDate = new Date(year, month, 29);
    // res = (month >= this.minDate.getMonth() && month <= this.maxDate.getMonth());
    const result = {
      res,
      minMonth,
      maxMonth,
      minDate,
      maxDate
    };
    return result;
  }


  getQuarters = () => {
    return this.quarters;
  }

}
