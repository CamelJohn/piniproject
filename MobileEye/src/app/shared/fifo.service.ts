import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FifoService {
@Output() localOrders = [];
overseasOrders = [];
localShirt: Observable<any> = this.db.collection('localShirt').valueChanges();
localExtras: Observable<any> = this.db.collection('localExtras').valueChanges();
overseasShirt: Observable<any> = this.db.collection('overseasShirt').valueChanges();
// quarters: Observable<any> = this.db.collection('quarters').valueChanges();

constructor(private db: AngularFirestore) {
}

  getlocalOrders() {
    return this.localOrders;
  }

  fifoSchedueling(orders) {
    // console.log(orders);
    orders.forEach(order => {
    // console.log(order);
      console.log(this.localOrders.length, this.overseasOrders.length);
      if (this.localOrders.length === 0 && this.overseasOrders.length === 0) {
        const tier = this.checkTier(order.tier);
        const type = this.checkType(order.shirtType);
        let costlocal = this.checkCost(tier, type, this.localShirt, 'local');
        let costoverseas = this.checkCost(tier, type, this.overseasShirt, 'overseas');

        costlocal *= order.orderQuanity;
        costoverseas *= order.orderQantity;

        if (costlocal >= costoverseas) {
          this.localOrders.push(order);
        } else {
          this.overseasOrders.push(order);
        }
        console.log(this.overseasOrders);
        // const costoverseas;
        'need to check who costs less';
      } else if (this.localOrders.length === 0 && this.overseasOrders.length !== 0) {
        'send to local';
      } else if (this.localOrders.length !== 0 && this.overseasOrders.length === 0) {
        'send to overseas';
      } else if (this.localOrders.length !== 0 && this.overseasOrders.length !== 0) {
        'check to see availabilitiy';
      }
    });
  }

  checkTier(order) {
    if (order.tier.includes('1')) {
      return order.tier;
    } else if (order.tier.includes('2')) {
      return order.tier;
    } else if (order.tier.includes('3')) {
      return order.tier;
    } else if (order.tier.includes('4')) {
      return order.tier;
    }
  }

  checkType(order) {
    if (order.shirtType.includes('Polo')) {
      return order.shirtType;
    } else if (order.shirtType.includes('Dress')) {
      return order.shirtType;
    } else if (order.shirtType.includes('American')) {
      return order.shirtType;
    } else if (order.shirtType.includes('T-Shirt')) {
      return order.shirtType;
    }
  }

  checkCost(tier, type, array, vendorType) {
    const vendor = array;
    let result = 0;
    vendor.forEach(el => {
      if (el.tier === tier && el.type === type && type.inclueds('Polo')) {
        result = el.poloCost;
      } else if (el.tier === tier && el.type === type && type.inclueds('Dress')) {
        result = el.dressShirtCost;
      } else if (el.tier === tier && el.type === type && type.inclueds('American')) {
        if (vendorType === 'overseas') {
          result = 0;
        } else {
          result = 0;
        }
      } else if (el.tier === tier && el.type === type && type.inclueds('T-Shirt')) {
        result = el.tShirtCost;
      }
    });
    return result;
  }

  checkOverseasCost() {

  }

}
