import { FifoService } from './../../shared/fifo.service';
import { OrderProcessingService } from './../../shared/order-processing.service';
import { DateService } from './../../shared/date.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class FormComponent implements OnInit {
  orderForm: FormGroup;

  constructor(
    private dateService: DateService,
    private orderProcessing: OrderProcessingService,
    private fifo: FifoService,
    private db: AngularFirestore
    ) { }
  shirtType: Observable<any> = this.db.collection('shirtType').valueChanges();
  bodyColor: Observable<any> = this.db.collection('bodyColor').valueChanges();
  sleeveColors: Observable<any> = this.db.collection('sleeveColors').valueChanges();
  perferences: Observable<any> = this.db.collection('preferences').valueChanges();
  extras1 = this.db.collection('extras1').valueChanges();
  extras: Observable<any> = this.db.collection('extras').valueChanges();
  // deliveryRange = this.db.collection('quarters', ref => ref.orderBy('id'));
  deliveryRange = this.dateService.getQuarters();
  // @Input() deliveryRange = this.fifo.getQuarters();
  maxDate;
  minDate;
  minMonth;
  maxMonth;
  dateRange;
  date: Date;
  pickedDate;
  orderNumber = 0;
  // @Output() allOrders = [];
  // orders: Observable<any>;
  orders = [];

  changeDateRange(range) {
    this.dateRange = range;
  }

  ngOnInit() {
    let localCost = this.fifo.getlocalOrders();
    console.log(localCost);
    // console.log(typeof this.deliveryRange, this.deliveryRange)
    // this.db
    //   .collection('fifoOrders')
    //   .valueChanges()
    //   .subscribe(result => {
    //     console.log(result);
    //   });
    // this.deliveryRange.subscribe(res => {
    //   for (const key of res) {
    //     console.log(key)
    //   }
    // });

    this.orderForm = new FormGroup({
      shirtTypeInput: new FormControl('', Validators.required),
      shirtBodyColorInput: new FormControl('', Validators.required),
      sleevesColorInput: new FormControl(''),
      preferencesInput: new FormControl('', Validators.required),
      orderQuantityInput: new FormControl('', Validators.required),
      extraFeaturesInput: new FormControl('', Validators.required),
      deliveryRangeInput: new FormControl('', Validators.required),
      deliveryDateInput: new FormControl('', Validators.required),
      customerName: new FormControl('', Validators.required),
    });
  }

  dateFilter = (d: Date): boolean => {
    // console.log(d);
    const result = this.dateService.myDateFilter(
      d , this.dateRange, this.deliveryRange, this.minMonth, this.maxMonth, this.minDate, this.maxDate);

    const res = result.res;
    this.minMonth = result.minMonth;
    this.maxMonth = result.maxMonth;
    this.minDate = result.minDate;
    this.maxDate = result.maxDate;
      // res = (month >= this.minDate.getMonth() && month <= this.maxDate.getMonth());
      // console.log(res);
    return res;
    }

    onSubmit() {
    console.log(this.orderForm.value.shirtTypeInput);
    const o = this.orderForm.value;
    const s = this.orderProcessing;
    // this.orderForm.reset(this.orderForm.value);
    const orderNum = this.orderNumber += 1;
    const order = {
      orderNumber: orderNum,
      shirtType: o.shirtTypeInput,
      bodyColor: o.shirtBodyColorInput,
      sleeveColor: o.sleevesColorInput,
      preferences: o.preferencesInput,
      orderQuanity: o.orderQuantityInput,
      extraFeatures: o.extraFeaturesInput,
      deliveryRange: o.deliveryRangeInput,
      deliveryDate: o.deliveryDateInput.toISOString().split('T')[0],
      receivedOrderDate: new Date().toISOString().split('T')[0],
      flexibleOn: s.checkPrefs(o.preferencesInput),
      rangeInDays: s.checkRange(o.deliveryDateInput),
      tier: s.checkTier(o.orderQuantityInput),
      customerName: o.customerName,
    };

    // this.allOrders.push(order);
    this.orders.push(order);
    this.fifo.fifoSchedueling(this.orders);
    // this.fifo.getFifo(order);
    console.log(this.orders);
  }

}
