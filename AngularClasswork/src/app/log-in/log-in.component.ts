import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  email: string;
  password: string;

  constructor(private authService: AuthService,
              private router: Router) { }

  onSubmit = () => {
    this.authService.login(this.email, this.password);
    this.router.navigate(['/books']);
  }

  ngOnInit() {
  }

}
