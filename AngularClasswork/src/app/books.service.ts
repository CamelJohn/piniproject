import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

//   books: any = [
//   {title: 'Alice in Wonderland', author: 'Lewis Carrol'},
//   {title: 'War and Peace', author: 'Leo Tolstoy'},
//   {title: 'The Magic Mountain', author: 'Thomas Mann'}
// ];

  // getBooks() {
  //   setInterval( () => {
  //     return this.books;
  //   }, 1000);
  // }

  // getBooks() {
  //   const bookObs = new Observable(
  //     observer => {
  //       setInterval(
  //         () => {
  //           return observer.next(this.books);
  //         }, 3000
  //       );
  //     }
  //   );
  //   return bookObs;
  // }

  // addBooks(title, author) {
  //   // setInterval( () => {
  //   //   return this.books.push({title: 'roni', author: 'is a stoner'});
  //   // }, 5000);
  //   return this.books.push({title, author});
  // }

  constructor(private db: AngularFirestore, private authService: AuthService) { }

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection;

  getBooks(): Observable<any[]> {
    // return this.db.collection(`users/${this.authService.key}`).valueChanges({ idField: 'key'});
    // return this.userCollection.doc(this.authService.key).collection('books').valueChanges({idField: 'key'});
    this.bookCollection = this.db.collection(`users/${this.authService.key}/books`);
    // console.log('Books collection created');
    return this.bookCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );
  }

  getBook(key: string): Observable<any> {
    return this.db.doc(`users/${this.authService.key}/books/${key}`).get();
  }

  addBooks(title: string, author: string) {
    const book = {
      title: title,
      author: author
    };

    // this.db.collection('books').add(book);
    this.userCollection.doc(this.authService.key).collection('books').add(book);
    // console.log(title, author, this.authService.key);
  }

  editBook(title: string, author: string, key: any) {

    // this.db.collection('books').doc(key).update({title: title, author: author});

    this.db.doc(`users/${this.authService.key}/books/${key}`).update(
      {
        title:title,
        author:author
      }
    )
  }

  deleteBook(key: any) {
    console.log(key);
    // this.db.collection('books').doc(key).delete();

    this.db.doc(`users/${this.authService.key}/books/${key}`).delete();
    // console.log('book with hash-key: ' + key + 'was deleted!');
  }
}
