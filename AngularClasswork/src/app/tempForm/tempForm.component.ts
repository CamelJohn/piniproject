import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  cities: object[] = [
    {id: 1, name: 'Tel Aviv'},
    {id: 1, name: 'London'},
    {id: 1, name: 'Hamburg'},
    {id: 1, name: 'Stalingrad'},
    {id: 1, name: 'Miami'},
    {id: 1, name: 'נתיב העשרה'},
    {id: 1, name: 'פרדס חנה'},
    {id: 1, name: 'Paris'},
    {id: 1, name: 'shitbrick'}];
  temperature: number;
  city: string;

  onSubmit() {
    // this.router.navigate(['/temperatures', this.temperature, this.city]);
    this.router.navigate(['/temperatures', this.city]);
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
