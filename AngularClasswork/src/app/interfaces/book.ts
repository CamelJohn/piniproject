export interface Book {
  bid: string,
  author: string,
  title: string,
}
