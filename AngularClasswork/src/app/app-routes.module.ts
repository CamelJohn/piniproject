import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { BookFormComponent } from './book-form/book-form.component';
import { TempformComponent } from './tempForm/tempForm.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { BooksComponent } from './books/books.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  { path: 'sign-up', component: SignUpComponent },
  { path: 'log-in', component: LogInComponent },
  { path: 'books', component: BooksComponent },
  { path: 'add-books', component: BookFormComponent },
  { path: 'edit-books/:title/:author/:key/edit', component: EditBookComponent },
  { path: 'delete-books/:key', component: EditBookComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'tempForm', component: TempformComponent },
  { path: '', redirectTo: '/books', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRouterModule {

}
