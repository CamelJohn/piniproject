import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  key;
  user: Observable<User | null>;

  constructor(public afAuth: AngularFireAuth, private route: Router) {
    this.user = this.afAuth.authState;
  }

  signUp = (email: string, password: string) => {
    this.afAuth
    .auth.createUserWithEmailAndPassword(email, password)
    .then(res => console.log('Successfull Signup', res))
    .catch(err => console.log(err));
  }

  Logout = () => {
    this.afAuth.auth.signOut();
    this.route.navigate(['/log-in'])
  }

  login = (email: string, password: string) => {
    this.afAuth
      .auth.signInWithEmailAndPassword(email, password)
      .then(res => {
        console.log('Successful Login', res);
        this.user.subscribe(user => this.key = user.uid);
      })
      .catch(err => console.log(err));
  }

}
