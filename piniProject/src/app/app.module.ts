import { SatisfactionService } from './shared/satisfaction.service';
import { HodgesonMoorService } from './shared/hodgeson-moor.service';
import { DashboardService } from './shared/dashboard.service';
import { EddService } from './shared/edd.service';
import { FirstCaseService } from './shared/first-case.service';
import { VendorsService } from './shared/vendors.service';
import { OrderService } from './shared/order.service';
import { FormService } from './shared/form.service';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormComponent } from './orders/form/form.component';
import { HeaderComponent } from './navbar/header/header.component';
import { SidenavListComponent } from './navbar/sidenav-list/sidenav-list.component';
import { FifoComponent } from './orders/fifo/fifo.component';
import { FifoService } from './shared/fifo.service';
import { EddComponent } from './orders/edd/edd.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HodgesonMoorComponent } from './orders/hodgeson-moor/hodgeson-moor.component';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    HeaderComponent,
    SidenavListComponent,
    FifoComponent,
    EddComponent,
    DashboardComponent,
    HodgesonMoorComponent,
    WelcomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
  ],
  providers: [
    FormService,
    OrderService,
    VendorsService,
    FifoService,
    FirstCaseService,
    EddService,
    DashboardService,
    HodgesonMoorService,
    SatisfactionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
