import { OrderService } from './order.service';
import { FormService } from './form.service';
import { Injectable } from '@angular/core';
// import { FifoService } from './fifo.service';
// import { VendorsService } from './vendors.service';

@Injectable({
  providedIn: 'root'
})
export class EddService {
  queue = [];
  localQ = [];
  chinaQ = [];
  local = this.formService.getLocal();
  china = this.formService.getChina();
  eddQ = [];
  newQ = [];

  min;
  max;
  lateness;
  totalLateness;
  cost;

  constructor(
    // private vendorService: VendorsService,
    private formService: FormService,
    // private fifo: FifoService
    private orders: OrderService
    ) { }

  getmin() {
    return this.min;
  }

  getmax() {
    return this.max;
  }

  getlatedays() {
    return this.lateness;
  }

  gettotallate() {
    return this.totalLateness;
  }

  getCost() {
    return this.cost;
  }

  setEddQ(q) {
    this.eddQ = q;
  }

  getStartingEddQ() {
    return this.eddQ;
  }

  getEddQ() {
    return this.queue;
  }

  setStartingQueue(queue) {
    this.queue = [...queue];
  }

  getStartingQueue() {
    return this.queue;
  }

  eddSort() {
    this.queue.forEach(order => {
      order.DueDate = order.desiredDeliveryDate.getTime() - order.dateRecieved.getTime();
    })
    this.queue.sort((a, b) => a.DueDate - b.DueDate);
    this.queue.forEach(order => {
      order.daysEarly = null;
      order.daysLate = null;
      order.isLate = true;
      order.forecastDeliveryDate = null;
      order.processTime = null;
      order.vendor = null;
      order.cost = 0;
      order.pricePerUnit = 0;
      order.PPUChina = 0;
      order.PPULocal = 0;
    });

    let costsObj = this.orders.determineCosts(this.queue, this.chinaQ, this.localQ);
    this.newQ = costsObj.newQ;
    this.setVendorQueue(this.newQ);
  }

    setVendorQueue(Q) {
      Q.forEach(el => {
        if (el == null) {

        } else {
          el.vendor === 'local' ? this.localQ.push(el) : this.chinaQ.push(el);
        }
      });
    }

    setFifoData(min, max, lateness, totalLateness, cost) {
      this.min = min;
      this.max = max;
      this.lateness = lateness;
      this.totalLateness = totalLateness;
      this.cost = cost;
    }
}
