import { FormService } from './form.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VendorsService {

  constructor(private formService: FormService) { }
  localAmerican = this.formService.getLocalAmerican();
  localVendor = this.formService.getLocal();
  localExtras = this.formService.getLocalExtras();
  chinaVendor = this.formService.getChina();
  chinaExtras = this.formService.getChinaExtras();

  localAmericanCost(data) {
    const res = { ppu: 0, cost: 0};
    this.localAmerican.forEach(row => {
        if (data.sleeveColor === row.sleeves && data.bodyColor === row.body) {
          res.ppu = row.cost;
          res.cost = row.cost * data.qty;
        }
      });
    return res;
  }

  chinaAmericanCost(data) {
    return data.qty * 3;
  }

  regularCost(data, vendor) {
    let cost = 0;
    let ppu = 0;

    if (vendor == null) {

    }

    vendor.forEach(el => {
      if (data.qty >= el.minQty
        && data.qty <= el.maxQty
        // && (data.shirtType === el.type || data.shirtType === 'American T-Shirt')
        && data.shirtType === el.type
        && data.bodyColor === el.color
        ) {
          cost = el.cost * data.qty;
          ppu = el.cost;
        }
    });
    return { cost, ppu };
  }

  extrasCost(data, extras) {
    let res = 0;
    extras.forEach(el => {
      if (data.extraFeatures === el.type) {
        res += el.cost;
      }
    });
    return res * data.qty;
  }

  calculateProcessTime(order, vendor, vendorQ) {
    let processTime;
    let deliveryDate;
    let last;
    let lastPt;

    if (vendor == null) {

    }

    vendor.forEach(venEl => {
      if (order.qty >= venEl.minQty &&
        order.qty <= venEl.maxQty &&
        order.shirtType === venEl.type
        && order.bodyColor === venEl.color) {
        processTime = venEl.processTime;
        if (vendorQ.length === 0) {
            deliveryDate = new Date(order.dateRecieved.setDate(order.dateRecieved.getDate() + venEl.processTime));
          }
        else if (vendorQ.length !== 0) {
          last = vendorQ.slice().pop();
          lastPt = last.dateReceived.setDate(last.dateReceived.getDate() + last.processTime);
          deliveryDate = new Date(lastPt.setDate(lastPt.getDate() + venEl.processTime));
        }

          }
    });
    return { processTime, deliveryDate };
  }
}
