import { FifoService } from './fifo.service';
import { EddService } from './edd.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(private edd: EddService, private fifo: FifoService) { }

  getEdd() {
    return this.fifo.getQ();
  }

  getFifo() {
    return this.edd.getEddQ();
  }


}
