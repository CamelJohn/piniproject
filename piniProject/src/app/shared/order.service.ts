import { FormService } from './form.service';
import { VendorsService } from './vendors.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  local = [];
  china = [];

  constructor(private vendorService: VendorsService, private formService: FormService) { }

  determineCosts(queue, chinaQ, localQ) {
    let localAmerican;
    let localRegular = 0;
    let localExtras = 0;
    let chinaAmerican = 0;
    let chinaRegular = 0;
    let chinaExtras = 0;
    let totalLocal = 0;
    let totalChineese = 0;
    let localRegularRes;
    let chinaRegularRes;
    let localPPU = 0;
    let chinaPPU = 0;
    let localAmericanCost = 0;
    let localAmericanPPU = 0;

    queue.forEach(data => {
      if (data.cost === 0) {
        if (data.shirtType === 'American T-Shirt') {
          localAmerican = this.vendorService.localAmericanCost(data);
          chinaAmerican = this.vendorService.chinaAmericanCost(data);
          localAmericanCost = localAmerican.cost;
          localAmericanPPU = localAmerican.ppu;
        }

        localExtras = this.vendorService.extrasCost(data, this.formService.getLocalExtras());
        chinaExtras = this.vendorService.extrasCost(data, this.formService.getChinaExtras());
        localRegularRes = this.vendorService.regularCost(data, this.formService.getLocal());
        chinaRegularRes = this.vendorService.regularCost(data, this.formService.getChina());
        localRegular = localRegularRes.cost;
        chinaRegular = chinaRegularRes.cost;
        localPPU = localRegularRes.ppu;
        chinaPPU = chinaRegularRes.ppu;

        totalLocal = localAmericanCost + localRegular + localExtras;
        totalChineese = chinaAmerican + chinaRegular + chinaExtras;

        if (totalLocal > totalChineese) {
          data.cost = totalChineese;
          data.pricePerUnit = chinaPPU;
          data.vendor = 'china';
          chinaQ.forEach(order => {
            if (order.barcode !== data.barcode && order.customer === data.customer) {
              chinaQ.push(data);
            }
          })
        } else {
          data.cost = totalLocal;
          data.pricePerUnit = localPPU;
          data.vendor = 'local';
          localQ.forEach(order => {
            if (order.barcode !== data.barcode && order.customer === data.customer) {
              localQ.push(data);
            }
          })
        }

        data.cCost = totalChineese;
        data.lCost = totalLocal;
        data.ppUChina = chinaPPU;
        data.ppULocal = localPPU;

      } else {
        data = data;
      }
    });

    let newQObj = this.determineDates(queue, chinaQ, localQ);
    let newQ = newQObj.newQueue;
    let newCQ = newQObj.chinaQ;
    let newLQ = newQObj.localQ;

    return { newQ, newCQ, newLQ };
  }

  determineDates(queue, chinaQ, localQ) {
    let newQueue = [];
    let local;
    let china;
    let localPT;
    let chinaPT;
    let localDD;
    let chinaDD;
    let result;

    queue.forEach(data => {
      localQ.forEach(order => {
        if (order.barcode !== data.barcode && order.customer === data.customer && data.vendor === 'local') {
          local = this.vendorService.calculateProcessTime(data, this.formService.getLocal(), localQ);
          localPT = local.processTime;
          localDD = local.deliveryDate;
        }
      });


      chinaQ.forEach(order => {
      if (order.barcode !== data.barcode && order.customer === data.customer && data.vendor === 'china') {
      china = this.vendorService.calculateProcessTime(data, this.formService.getChina(), chinaQ);
      chinaPT = china.processTime;
      chinaDD = china.deliveryDate;
      }
    })

      if (data.vendor === 'china') {
        result = this.setDateData(data, 'china', chinaPT, chinaDD, localDD, chinaDD);
        chinaQ.forEach(order => {
          if (order.barcode !== data.barcode && order.customer === data.customer) {
            chinaQ.push(data);
          }
        })
      } else if (data.vendor === 'local') {
        result = this.setDateData(data, 'local', localPT, localDD, localDD, chinaDD);

        localQ.forEach(order => {
          if (order.barcode !== data.barcode && order.customer === data.customer) {
            localQ.push(result);
          }
        })
        }


      this.china = chinaQ;
      this.local = localQ;

      newQueue.push(result);
  });
    return { newQueue, chinaQ, localQ };
  }

    private setDateData(data, vendor, PT, DD, lDD, cDD) {
      data.vendor = vendor;
      data.processTime = PT;
      data. forecastDeliveryDate = DD;
      const date = this.checkTardiness(data.forecastDeliveryDate, data.desiredDeliveryDate);
      if (data.forecastDeliveryDate > data.desiredDeliveryDate) {
        data.daysLate = date * -1;
        data.daysEarly = 0;
        data.isLate = true;
      } else {
        data.daysLate = 0;
        data.daysEarly = date;
        data.isLate = false;
      }
      console.log(cDD);
      console.log(lDD);
      data.dChina = cDD;
      data.dLocal = lDD;
      return data;
    }

    checkTardiness(fd: Date, dd: Date) {
      fd = new Date(fd);
      dd = new Date(dd);
      return Math.floor(
        (Date.UTC(dd.getFullYear(), dd.getMonth(), dd.getDate())
        - Date.UTC(fd.getFullYear(), fd.getMonth(), fd.getDate()) ) / (1000 * 60 * 60 * 24));
    }
}

