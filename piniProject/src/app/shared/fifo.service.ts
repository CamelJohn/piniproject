import { FirstCaseService } from './first-case.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FifoService {

  constructor(private firstCase: FirstCaseService) { }
  queue = [];
  china = [];
  local = [];

  storeAllDataLocaly(queue, china, local) {
    this.queue = [...queue];
    this.local = [...local];
    this.china = [...china];
  }

  getQ() {
    return this.firstCase.getFirstCase();
  }

  getChina() {
    return this.china;
  }

  getLocal() {
    return this.local;
  }
}
