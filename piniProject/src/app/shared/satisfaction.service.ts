import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SatisfactionService {

  constructor() { }

claculateSatisfaction(queue) {

  queue.forEach(order => {
      if (order.preferences === 'None') {
        order.pricePercent = 0.33;
        order.deliveryPercent = 0.34;
        order.pricePerUnitPercent = 0.33;
      } else if (order.preferences === 'flexible on qantity & colors for lower price' ||
        order.preferences === 'flexible on delivery date for lower price') {
        order.pricePercent = 0.75;
        order.deliveryPercent = 0.20;
        order.pricePerUnitPercent = 0.05;
      } else if (order.preferences === 'flexible on colors for quicker delivery') {
        order.pricePercent = 0.20;
        order.deliveryPercent = 0.75;
        order.pricePerUnitPercent = 0.05;
      } else if (order.preferences === 'bigger order quantity for lower price per unit' ||
        order.preferences === 'flexible on delivery date for lower price per unit' ||
        order.preferences === 'flexible on colors for lower price per unit') {
        order.pricePercent = 0.20;
        order.deliveryPercent = 0.05;
        order.pricePerUnitPercent = 0.75;
      }
      if (order.vendor === 'china') {
        order.pricePerUnitRatio = (order.ppUChina / this.defaultZero(order.ppULocal));
        order.deliveryRatio = (order.dChina / (order.dLocal));
        order.priceRatio = (order.cCost / this.defaultZero(order.lCost));
      } else if (order.vendor === 'local') {
        order.pricePerUnitRatio = (order.ppULocal / this.defaultZero(order.ppUChina));
        order.deliveryRatio = (order.dLocal / (order.dChina));
        order.priceRatio  = (order.lCost / this.defaultZero(order.cCost));

      }
      order.satisfaction = +(order.pricePercent * order.priceRatio +
      order.deliveryPercent * order.deliveryRatio + order.pricePerUnitPercent * order.pricePerUnitRatio).toFixed(3);

      if (order.satisfaction > 1) {
        order.satisfaction = (1 - (order.satisfaction - 1)).toFixed(3);
        if (order.satisfaction < 0) {
          order.satisfaction = order.satisfaction * -1;
        }
      } else if (order.satisfaction > 2) {
        order.satisfaction = (1 - (order.satisfaction - 2)).toFixed(3);
      } else if (order.satisfaction < 0.5) {
        order.satisfaction = (1 - order.satisfaction).toFixed(3)
      }
    });
  return queue;
  }

  defaultZero(sub) {
    let res = 0;
    sub === 0 ? res = 100 : res = sub;
    return res;
  }
}
