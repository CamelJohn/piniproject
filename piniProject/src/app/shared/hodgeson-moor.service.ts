import { EddService } from './edd.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HodgesonMoorService {

  constructor(private edd: EddService) { }



  getQ() {
    return this.edd.getStartingEddQ();
  }

  // setFifoQ(q) {
  //   const queue = this.findLate(q);
  //   this.queue.push(queue);
  //   this.queue.push(...this.tardy);
  // }

  // findLate(q) {

  //   const found = q.find((el) => {
  //     return el.isLate === true;
  //   });

  //   if (found) {
  //     q.filter(t => t === found);
  //     this.tardy.push(found);
  //     q = this.reorder(q);
  //   } else {
  //     return q;
  //   }
  // }

  // reorder(q) {
  //   q.sort((a, b) => b.desiredDeliveryDate - a.desiredDeliveryDate);
  //   this.findLate(q);
  // }



  // getFifoQ() {
  //   return this.fifoQ;
  // }
}
