import { DashboardService } from '../shared/dashboard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  edd = [];
  fifo = [];
  constructor(private ds: DashboardService) { }

  ngOnInit() {
    this.edd = this.ds.getEdd();
    this.fifo = this.ds.getFifo();
  }

  sum(key, array) {
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }

  sumLateJobs(queue) {
    let lateCount = 0;
    queue.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness(queue) {
    const latenssArr = [];
    let max = 0;
    queue.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }


}
