import { SatisfactionService } from '../../shared/satisfaction.service';
import { VendorsService } from '../../shared/vendors.service';
import { EddService } from '../../shared/edd.service';
import { FirstCaseService } from '../../shared/first-case.service';
import { FifoService } from '../../shared/fifo.service';
import { OrderService } from '../../shared/order.service';
import { FormService } from '../../shared/form.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  orderForm: FormGroup;
  isAmerican = false;
  maxDate;
  minDate;
  date: Date;
  orderNumber = 20;
  orders = [];
  shirtType;
  bodyColor;
  sleeveColors;
  perferences;
  extras;
  deliveryRange;
  localQueue = [];
  chinaQueue = [];

  constructor(
    private formService: FormService,
    private orderService: OrderService,
    private fifo: FifoService,
    private first: FirstCaseService,
    private edd: EddService,
    private vendorService: VendorsService,
    private s: SatisfactionService
  ) { }

  ngOnInit() {
    this.shirtType = this.formService.getShirtType();
    this.bodyColor = this.formService.getShirtColor();
    this.sleeveColors = this.formService.getSleeveColor();
    this.perferences = this.formService.getPreferences();
    this.deliveryRange = this.formService.getDeliveryRange();
    this.extras = this.formService.getExtraFeatures();
    if (this.orders.length === 0) {
      this.orders = this.first.getFirstCase();
    } else {
      this.orders = this.fifo.getQ();
    }

    this.orderForm = new FormGroup({
      shirtTypeInput: new FormControl('', ),
      shirtBodyColorInput: new FormControl('', ),
      sleevesColorInput: new FormControl(''),
      preferencesInput: new FormControl('', ),
      orderQuantityInput: new FormControl('', ),
      extraFeaturesInput: new FormControl('', ),
      deliveryRangeInput: new FormControl('', ),
      deliveryDateInput: new FormControl('', ),
      customerNameInput: new FormControl('', ),
    });

    this.orderForm.get('deliveryRangeInput').valueChanges.subscribe(
      (range) => {
        this.deliveryRange.forEach(el => {
          if (range === el.quarter) {
            this.minDate = el.min;
            this.maxDate = el.max;
          }
        });
      }
      );

    this.orderForm.get('shirtTypeInput').valueChanges.subscribe((type) => {
        if (type === 'American T-Shirt') {
          this.orderForm.get('sleevesColorInput').enable();
          this.isAmerican = true;
          this.extras = this.extras.filter(e => e.name !== 'Collar Buttons');
        } else {
          this.isAmerican = false;
          this.orderForm.get('sleevesColorInput').disable();
          if (type === 'Dress Shirt') {
            this.extras = this.formService.getExtraFeatures();
          } else {
            this.extras = this.extras.filter(e => e.name !== 'Collar Buttons');
          }
        }
      });

    this.orderForm.get('shirtBodyColorInput').valueChanges.subscribe((color) => {
      this.sleeveColors = this.formService.getSleeveColor();
      this.sleeveColors = this.isAmerican ? this.sleeveColors.filter(c => c.color !== color) : this.formService.getSleeveColor();
      });
  }

  onSubmit() {
    this.orderNumber += 1;
    const order = {
      barcode: this.orderNumber,
      customer: this.orderForm.value.customerNameInput,
      shirtType: this.orderForm.value.shirtTypeInput,
      bodyColor: this.orderForm.value.shirtBodyColorInput,
      sleeveColor: this.orderForm.value.sleevesColorInput == null ?
      this.orderForm.value.shirtBodyColorInput : this.orderForm.value.sleevesColorInput,
      preferences: this.orderForm.value.preferencesInput,
      qty: this.orderForm.value.orderQuantityInput,
      extraFeatures: this.orderForm.value.extraFeaturesInput,
      desiredDeliveryDate: this.orderForm.value.deliveryDateInput,
      desiredDeliverRange: this.orderForm.value.deliveryRangeInput,
      dateRecieved: new Date(),
      DueDate: null,
      daysEarly: null,
      daysLate: null,
      isLate: null,
      forecastDeliveryDate: null,
      processTime: null,
      vendor: null,
      cost: 0,
      cCost: 0,
      lCost: 0,
      pricePerUnit: 0,
      ppUChina: 0,
      ppULocal: 0,
      dChina: 0,
      dLocal: 0,
      pricePercent: 0,
      deliveryPercent: 0,
      pricePerUnitPercent: 0,
      satisfaction: 0,
      isSimulation: false,
      pricePerUnitRatio: 0,
      deliveryRatio: 0,
      priceRatio: 0
    };
    this.orderForm.reset();
    if (order.customer === '') {

    } else {
      this.orders.push(order);
    }
    const lastOrder = this.last(this.orders);
    console.log(lastOrder);
    // this.orders.pop();
    // this.orders = this.orderService.determineDates(this.orders, this.chinaQueue, this.localQueue);
    let costObj = this.orderService.determineCosts(this.orders, this.chinaQueue, this.localQueue);
    this.orders = costObj.newQ;
    this.localQueue = costObj.newLQ;
    this.chinaQueue = costObj.newCQ;
    this.orders = this.s.claculateSatisfaction(this.orders);
    // this.setVendorQueue(this.orders);
    // this.fifo.storeAllDataLocaly(this.orders, this.chinaQueue, this.localQueue);
    console.log(this.localQueue);
    console.log(this.chinaQueue);
    // this.edd.setStartingQueue(this.orders);
  }

  dateFilter = (d: Date): boolean => {
    const month = d.getMonth();
    const res = (month >= this.minDate - 1 && month <= this.maxDate - 1 && month >= new Date().getMonth());
    return res;
  }

  sum(key, array) {
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  setVendorQueue(Q) {
    Q.forEach(el => {
      if (el == null) {

      } else {
        el.vendor === 'local' ? this.localQueue.push(el) : this.chinaQueue.push(el);
      }
    });
  }

  last(array) {
    if (array == null) {
      return void 0;
    }
    return array.slice(Math.max(array.length - 3, 0));
  }
}
