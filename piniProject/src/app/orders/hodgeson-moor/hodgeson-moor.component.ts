import { OrderService } from './../../shared/order.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { HodgesonMoorService } from '../../shared/hodgeson-moor.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-hodgeson-moor',
  templateUrl: './hodgeson-moor.component.html',
  styleUrls: ['./hodgeson-moor.component.css']
})
export class HodgesonMoorComponent implements OnInit {

  constructor(
    private hm: HodgesonMoorService,
    private orders: OrderService) { }

  displayedColumns = ['orderId', 'customer', 'type', 'qty', 'dd', 'fd', 'tardiness', 'vendor', 'cost', 'satisfaction'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  queue = [];
  chinaQ = [];
  localQ = [];
  lateArr = [];

  ngOnInit() {
    this.queue = this.hm.getQ();
    this.queue = this.eddSort(this.queue);
    this.findFirsLateJob(this.queue);
  }

  eddSort(queue) {
    queue.sort((a, b) => a.desiredDeliveryDate - b.desiredDeliveryDate);
    queue.forEach(order => {
      order.daysEarly = null;
      order.daysLate = null;
      order.isLate = true;
      order.forecastDeliveryDate = null;
      order.processTime = null;
      order.vendor = null;
      order.cost = 0;
      order.pricePerUnit = 0;
      order.PPUChina = 0;
      order.PPULocal = 0;
    });
    queue = this.orders.determineDates(this.queue, this.chinaQ, this.localQ);
    this.setVendorQueue(queue);
    return queue;
  }

  setVendorQueue(Q) {
    Q.forEach(el => {
      if (el == null) {

      } else {
        el.vendor === 'local' ? this.localQ.push(el) : this.chinaQ.push(el);
      }
    });
  }

  findFirsLateJob(queue) {
    let flag = false;
    let lateId = 0;
    let lateLocation = 0;
    queue.forEach((order, id) => {
      id++;
      if (!flag) {
        if (order.isLate) {
          flag = true;
          lateId = order.barcode;
          lateLocation = id;
          // this.lateArr.push(order);
        }
      }
    });

    const tjComparisonArr = [];

    queue.forEach((order, id) => {
      if (id <= lateLocation) {
        tjComparisonArr.push(order);
      }
    });
    console.log(tjComparisonArr)
    // this.queue = queue.filter(o => o.barcode !== lateId);
    console.log(this.queue);
  }

  checkBeforeLateJob() {
    // check jobs before late job for highest tj
  }

  moveTolateArr() {
    // movechosen to lateArr
  }

  combineAll() {
    // combine Arr and LateArr togather and send
  }

  hmSort(queue) {
    queue = this.eddSort(queue);

  }
}
