import { SatisfactionService } from '../../shared/satisfaction.service';
import { HodgesonMoorService } from '../../shared/hodgeson-moor.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { EddService } from '../../shared/edd.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-edd',
  templateUrl: './edd.component.html',
  styleUrls: ['./edd.component.css']
})
export class EddComponent implements OnInit {
  displayedColumns = ['orderId', 'customer', 'type', 'qty', 'dd', 'fd', 'tardiness', 'vendor', 'cost', 'satisfaction'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  queue = this.edd.getStartingQueue();
  constructor(
    private edd: EddService,
    private s: SatisfactionService
    ) { }
    maxSatis;
    minSatis;
    lateness;
    totalLateness;
    cost;

  ngOnInit() {
    this.maxSatis = this.edd.getmax();
    this.minSatis = this.edd.getmin();
    this.lateness = this.edd.getlatedays();
    this.totalLateness = this.edd.gettotallate();
    this.cost = this.edd.getCost();

    this.edd.eddSort();
    this.queue = this.edd.getStartingQueue();
    this.queue = this.s.claculateSatisfaction(this.queue);
    console.log(this.queue);
    this.dataSource.data = this.queue;
    this.dataSource.paginator = this.paginator;
    this.edd.setEddQ(this.queue);
  }

  sum(key, array) {
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }

  sumLateJobs() {
    let lateCount = 0;
    this.queue.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness() {
    const latenssArr = [];
    let max = 0;
    this.queue.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

  maxSatisfaction() {
    const satisfactionArr = [];
    let max = 0;
    this.queue.forEach(el => {
      satisfactionArr.push(el.satisfaction);
    });
    if (satisfactionArr.length === 0) {
      max = 0;
    } else {
      max = satisfactionArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

  minSatisfaction() {
    const satisfactionArr = [];
    let max = 0;
    this.queue.forEach(el => {
      satisfactionArr.push(el.satisfaction);
    });
    if (satisfactionArr.length === 0) {
      max = 0;
    } else {
      max = satisfactionArr.reduce((a, b) => {
        return Math.min(a, b);
      });
    }
    return max;
  }
}
