import { OrderService } from './../../shared/order.service';
import { FirstCaseService } from './../../shared/first-case.service';
import { SatisfactionService } from '../../shared/satisfaction.service';
import { EddService } from '../../shared/edd.service';
import { FifoService } from '../../shared/fifo.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-fifo',
  templateUrl: './fifo.component.html',
  styleUrls: ['./fifo.component.css']
})
export class FifoComponent implements OnInit {
  displayedColumns = ['orderId', 'customer', 'type', 'qty', 'dd', 'fd', 'tardiness', 'vendor', 'cost', 'satisfaction'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(
    private fifo: FifoService,
    private edd: EddService,
    private s: SatisfactionService,
    private firstCase: FirstCaseService,
    private order: OrderService) { }

  local = this.fifo.getLocal();
  china = this.fifo.getChina();
  queue = [];
  queueWithSatifaction = [];

  ngOnInit() {
    let costObj = this.order.determineCosts(this.firstCase.getFirstCase(), this.china, this.local);
    this.queue = costObj.newQ;
    this.queueWithSatifaction = this.s.claculateSatisfaction(this.queue);
    console.log(this.queueWithSatifaction)
    this.dataSource.data = this.queueWithSatifaction;
    this.dataSource.paginator = this.paginator;
    this.edd.setStartingQueue(this.queue);
    this.edd.setFifoData(
      this.minSatisfaction(),
      this.maxSatisfaction(),
      this.maxLateness(),
      this.sum('daysLate', this.queue),
      this.sum('cost', this.queue)
      );
  }

  sum(key, array) {
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }

  sumLateJobs() {
    let lateCount = 0;
    this.queue.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness() {
    const latenssArr = [];
    let max = 0;
    this.queue.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

  maxSatisfaction() {
    const satisfactionArr = [];
    let max = 0;
    this.queue.forEach(el => {
      satisfactionArr.push(el.satisfaction);
    });
    if (satisfactionArr.length === 0) {
      max = 0;
    } else {
      max = satisfactionArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

  minSatisfaction() {
    const satisfactionArr = [];
    let max = 0;
    this.queue.forEach(el => {
      satisfactionArr.push(el.satisfaction);
    });
    if (satisfactionArr.length === 0) {
      max = 0;
    } else {
      max = satisfactionArr.reduce((a, b) => {
        return Math.min(a, b);
      });
    }
    return max;
  }




}
