import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'piniProject';
  url = false;

  constructor(private router: Router) {
    router.events.subscribe(event => {

      if (event instanceof NavigationEnd ) {

        if (event.url === '/') {
          this.url = true;
        } else {
          this.url = false;
        }
        // console.log("current url",event.url); // event.url has current url
        // your code will goes here
      }
    });
  }


}
