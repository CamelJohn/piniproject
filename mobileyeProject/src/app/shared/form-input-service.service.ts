import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormInputServiceService {

  constructor() { }

  private colors = ['Black', 'White', 'Grey', 'Blue', 'Light Blue', 'Red', 'Yellow'];
  private type = ['Polo', 'Dress Shirt', 'T-Shirt', 'American T-Shirt'];
  private sleeves = ['Black', 'Blue', 'White', 'Yellow'];
  // private extras1 = ['Left Pocket', 'Right Pocket'];
  private extras = ['Left Pocket', 'Right Pocket', 'Collar Buttons'];
  // private extras;
  // private isAmerican = false;
  private preferences = [
    '- none -',
    'Flexible color for quicker delivery',
    'Flexible color for lower price per unit',
    'Flexible qantity & color for lower price',
    'Larger order for lower price per unit',
    'Flexible date for lower price reduction',
    'Flexible date for lower price per unit',
    ];

  // getChangedExtras(type) {
  //   if (type !== 'American T-Shirt') {
  //     this.extras = this.extras1;
  //     this.isAmerican = true;
  //   } else {
  //     this.extras = this.extras2;
  //     this.isAmerican = true;
  //   }
  // }

  // getIsAmerican() {
  //   return this.isAmerican;
  // }

  getBodyColors() {
    return this.colors;
  }

  getShirtType() {
    return this.type;
  }

  getSleeveColors() {
    return this.sleeves;
  }

  getExtras() {
    return this.extras;
  }

  getPreferences() {
    return this.preferences;
  }
}
