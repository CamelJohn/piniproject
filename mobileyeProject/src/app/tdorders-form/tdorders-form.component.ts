import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { FormInputServiceService } from '../shared/form-input-service.service';
import { OrdersService } from '../shared/orders.service';

@Component({
  selector: 'app-tdorders-form',
  templateUrl: './tdorders-form.component.html',
  styleUrls: ['./tdorders-form.component.css']
})
export class TDordersFormComponent implements OnInit {
  @ViewChild('f', { static: false }) ordersForm: NgForm;

  bodyColor = this.inputService.getBodyColors();
  types = this.inputService.getShirtType();
  SleeveColor = this.inputService.getSleeveColors();
  extras = this.inputService.getExtras();
  preferences = this.inputService.getPreferences();

  selectedType: String = 'Select a shirt type';
  // isAmerican = this.inputService.getIsAmerican();

  order = {
    shirtType: '',
    ordeQuantity: '',
    bodyColor: '',
    sleeveColor: '',
    extras: '',
    preferences: '',
    deliveryDate: '',
  };

  orders = [];

  submitted = false;

  constructor(private inputService: FormInputServiceService, private ordersService: OrdersService) { }

  // changedType(type) {
  //   this.inputService.getChangedExtras(type);
  // }


  // showHideSeeveColors() {
  //   if (this.isAmerican) {

  //   } else {

  //   }
  // }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;

    const order = {
      shirtType: this.ordersForm.value.ShirtType,
      ordeQuantity: this.ordersForm.value.qty,
      bodyColor: this.ordersForm.value.BodyColor,
      sleeveColor: this.ordersForm.value.SleeveColor,
      extras: this.ordersForm.value.extraFeatures,
      preferences: this.ordersForm.value.preferences,
      deliveryDate: this.ordersForm.value.date,
    };
    // this.order.shirtType = this.ordersForm.value.ShirtType;
    // this.order.ordeQuantity = this.ordersForm.value.qty;
    // this.order.bodyColor = this.ordersForm.value.BodyColor;
    // this.order.sleeveColor = this.ordersForm.value.SleeveColor;
    // this.order.extras = this.ordersForm.value.extraFeatures;
    // this.order.preferences = this.ordersForm.value.preferences;
    // this.order.deliveryDate = this.ordersForm.value.date;

    // console.log(this.order, 'before');
    // if (this.orders.length === 0) {
    //   this.orders.push(this.order);
    // } else {
    //   const ordersNew = this.ordersService.getOrdersEdd(this.orders, this.order);
    //   this.orders = ordersNew;
    // }
    this.orders.push(order);
    if (this.orders.length > 1) {
      this.orders = this.ordersService.reorderOrders(this.orders);
    }
    // for (let i = 0; i < this.orders.length; i++) {
    //   console.log(i, this.orders[i].deliveryDate);
    // }

    this.ordersForm.reset();
  }



}
